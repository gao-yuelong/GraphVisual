﻿#ifndef COMMON_H
#define COMMON_H

//该文件定义一些全局的类型

enum E_CURSOR_STATE {
    eNormal,
    eDrawLine
};

//结点图片路径
#define NODEPIXMAPPATH Qt::UserRole + 1
//结点图片名称
#define NODEPIXMAPNAME Qt::UserRole + 2
//结点名称
#define NODENAME Qt::UserRole + 3


#endif // COMMON_H
