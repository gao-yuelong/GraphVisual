@echo off
echo "translate begin..."

set QTDIRPATH=D:\qt\qtinstall\5.12.0\msvc2015_64
REM set PYTHONPATH=python install dir
set PYTHONPATH=python
set BATPATH=%~dp0
echo %QTDIRPATH%

%QTDIRPATH%\bin\lupdate.exe -version

if not exist %QTDIRPATH%\bin\lupdate.exe (
    echo "config QTDIR first"
    pause
)

if exist %BATPATH%en_to_zh.ts del %BATPATH%en_to_zh.ts /q
if exist %BATPATH%en_to_zh.qm del %BATPATH%en_to_zh.qm /q

%QTDIRPATH%\bin\lupdate.exe %BATPATH%..\..\GraphVisual.pro -ts %BATPATH%en_to_zh.ts

%PYTHONPATH% %BATPATH%translate-py3.py

%QTDIRPATH%\bin\lrelease.exe %BATPATH%en_to_zh.ts -qm %BATPATH%en_to_zh.qm

copy %BATPATH%en_to_zh.qm %BATPATH%..\..\resource\ /y
echo "translate end..."
pause