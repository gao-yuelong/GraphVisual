#-------------------------------------------------
#
# Project created by QtCreator 2021-12-23T22:09:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GraphVisual
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

RC_ICONS = graphvisual.ico

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

INCLUDEPATH += \
            graph\
            include

SOURCES += \
    core/edge.cpp \
    core/node.cpp \
    graph/customellipseitem.cpp \
    graph/garden.cpp \
    src/log.cpp \
        system\main.cpp \
        graph\graphvisual.cpp \
    graph\customlistwidget.cpp \
    graph\customscene.cpp \
    graph\customview.cpp \
    graph\custompixmapitem.cpp \
    graph\customlineitem.cpp \
    graph\menu.cpp \
    graph\leftsidebar.cpp

HEADERS += \
    core/edge.h \
    core/node.h \
    graph/customellipseitem.h \
    graph/garden.h \
        graph\graphvisual.h \
    graph\customlistwidget.h \
    graph\customscene.h \
    graph\customview.h \
    graph\custompixmapitem.h \
    graph\customlineitem.h \
    include\common.h \
    graph\menu.h \
    graph\leftsidebar.h \
    src/log.h

FORMS += \
        graph\graphvisual.ui \
    graph\menu.ui \
    graph\leftsidebar.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    res.qrc
