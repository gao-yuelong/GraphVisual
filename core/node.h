﻿#ifndef NODE_H
#define NODE_H

#include "edge.h"

#include <QPoint>
#include <QSet>

class Edge;
//结点类
class Node
{
public:
    Node();
    Node(int iId);
    int getNodeId() {
        return m_iId;
    }
    void setNodeId(int iNodeId) {
        m_iId = iNodeId;
    }
    void setPos(QPoint point) {
        m_pointPos = point;
    }


private:
    int m_iId;
    QPoint m_pointPos;

    QSet<Edge *> m_setEdge;
};

#endif // NODE_H
