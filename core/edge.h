﻿#ifndef EDGE_H
#define EDGE_H

#include <QPoint>

//边
class Edge
{
public:
    Edge(int iId);

    void setStartPos(QPoint pointStart) {
        m_pointStart = pointStart;
    }
    void setEndPos(QPoint pointEnd) {
        m_pointEnd = pointEnd;
    }

    int getEdgeId() {
        return m_iId;
    }

private:
    int m_iId;

    QPoint m_pointStart;
    QPoint m_pointEnd;
};

#endif // EDGE_H
