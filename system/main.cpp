﻿#include "graphvisual.h"
#include "garden.h"
#include "src/log.h"
#include <QApplication>
#include <QTranslator>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    LOG_INFO("welcome to use GraphVisual")

    QTranslator translator;

    translator.load("en_to_zh.qm", ":/resource/");

    a.installTranslator(&translator);

    GraphVisual w;
    w.showMaximized();

    return a.exec();
}
