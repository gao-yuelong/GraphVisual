﻿#ifndef LOG_H
#define LOG_H

#define WRITELOG(logType, strLog) Log::getInstance()->write(logType, strLog);

#define LOG_INFO(strLog) WRITELOG(Log::E_LogType::eInfo, strLog)
#define LOG_ERROR(strLog) WRITELOG(Log::E_LogType::eError, strLog)

#include <QFile>
class QString;

class Log
{
public:
    static Log* getInstance()
    {
        static Log log;
        return &log;
    }

    enum E_LogType {
        eInfo,
        eError
    };

    void write(E_LogType, QString);

private:
    Log();
    ~Log();

    void init();
private:
    QFile m_file;
};

#endif // LOG_H
