﻿#include "log.h"

#include <QFile>
#include <QDateTime>
#include <QDebug>

Log::Log() : m_file("./log")
{
    init();
}

Log::~Log()
{
    m_file.close();
}

void Log::init()
{
    m_file.open(QIODevice::Append);
}

void Log::write(E_LogType eLogType, QString strMsg)
{
    QString strLog;
    switch (eLogType) {
    case E_LogType::eInfo:
        strLog += "[INFO]";
        break;
    case E_LogType::eError:
        strLog += "[ERROR]";
        break;
    default:
        break;
    }
    strLog += "[" + QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss") + "] : ";
    strLog += strMsg;
    strLog += "\n";
    qDebug() << strLog;
    m_file.write(strLog.toLocal8Bit());
    m_file.flush();
}
