﻿#include "custompixmapitem.h"
#include "common.h"
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QMenu>
#include <QDebug>

#define PIXMAPITEMWIDTH 40
#define PIXMAPITEMHEIGHT 60

CustomPixmapItem::CustomPixmapItem()
    : m_bIsStart(false)
    , m_bIsEnd(false)
{
    setFlag(QGraphicsItem::ItemIsMovable, true);
    setFlag(QGraphicsItem::ItemIsSelectable, true);
    setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
}

QRectF CustomPixmapItem::boundingRect() const
{
    //return QGraphicsPixmapItem::boundingRect();
    return QRectF(0, 0, PIXMAPITEMWIDTH, PIXMAPITEMHEIGHT);
}

QPainterPath CustomPixmapItem::shape() const
{
    return QGraphicsPixmapItem::shape();
}

bool CustomPixmapItem::contains(const QPointF &point) const
{
    return QGraphicsPixmapItem::contains(point);
}

void CustomPixmapItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    //return QGraphicsPixmapItem::paint(painter, option, widget);
    painter->drawPixmap(QPoint(3, 4), this->data(NODEPIXMAPPATH).toString());
    if (option->state & QStyle::State_Selected) {
        QRectF rect = getItemRect();
        QColor color = QColor(Qt::red);
        painter->setPen(QPen(color));
        painter->setBrush(Qt::NoBrush);

        //rect.adjust(-15, -15, 15, 15);
        painter->drawEllipse(rect);
    }
    painter->drawText(QRectF(5, 42, 40, 10), this->data(NODENAME).toString());
}

bool CustomPixmapItem::isObscuredBy(const QGraphicsItem *item) const
{
    return QGraphicsPixmapItem::isObscuredBy(item);
}

QPainterPath CustomPixmapItem::opaqueArea() const
{
    return QGraphicsPixmapItem::opaqueArea();
}

void CustomPixmapItem::startRun()
{
    //开始run
    //遍历以该结点为起点的所有线
    foreach (CustomLineItem *pLineItem, m_vecStartLines) {
        pLineItem->drawAnimation();
    }
}

void CustomPixmapItem::mousePressEvent(QGraphicsSceneMouseEvent *pEvent)
{
    //右键弹出菜单
    if (pEvent->button() & Qt::RightButton) {
        this->setSelected(true);
        QMenu menu;
        menu.addAction(tr("Set Start"), [this](){
            emit signalSetStart();
            setStart();
        });
        menu.addAction(tr("Set End"), [this](){
            emit signalSetEnd();
            setEnd();
        });
        menu.exec(QCursor::pos());
    }
    this->setSelected(false);
}

QVariant CustomPixmapItem::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    if (change == QGraphicsItem::ItemPositionChange) {
        foreach (CustomLineItem *pLineItem, m_vecStartLines) {
            pLineItem->updateLineItem();
        }
        foreach (CustomLineItem *pLineItem, m_vecEndLines) {
            pLineItem->updateLineItem();
        }
    }
    return value;
}

void CustomPixmapItem::setPixItemId(int iId)
{
    //m_pNode->setNodeId(iId);
}

int CustomPixmapItem::getPixItemId()
{
    return /*m_pNode->getNodeId()*/0;
}

void CustomPixmapItem::addLine(CustomLineItem *pLineItem)
{
    m_vecLinesItem.push_back(pLineItem);
}

void CustomPixmapItem::addStartLine(CustomLineItem *pStartLine)
{
    m_vecStartLines.append(pStartLine);
}

void CustomPixmapItem::addEndLine(CustomLineItem *pEndLine)
{
    m_vecEndLines.append(pEndLine);
}

QVector<CustomLineItem *> CustomPixmapItem::getLines()
{
    return m_vecLinesItem;
}

//获取当前item rect
QRectF CustomPixmapItem::getItemRect() const
{
    QPointF centerPos(0,0);
    return QRectF(0, 0, 40, 40);
}
