#include "leftsidebar.h"
#include "ui_leftsidebar.h"

LeftSideBar::LeftSideBar(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LeftSideBar)
{
    ui->setupUi(this);
}

LeftSideBar::~LeftSideBar()
{
    delete ui;
}
