﻿#ifndef CUSTOMELLIPSEITEM_H
#define CUSTOMELLIPSEITEM_H

#include <QGraphicsEllipseItem>

class CustomEllipseItem : public QGraphicsEllipseItem
{
public:
    CustomEllipseItem();
};

#endif // CUSTOMELLIPSEITEM_H
