﻿#ifndef CUSTOMLINEITEM_H
#define CUSTOMLINEITEM_H

#include <QGraphicsLineItem>
#include <QGraphicsItemAnimation>
#include <QTimeLine>
#include <QObject>
#include <QPointF>

class CustomPixmapItem;
class CustomEllipseItem;

//边
class CustomLineItem : public QObject, public QGraphicsLineItem
{
    Q_OBJECT
public:
    enum { Type = UserType + 2 };
    CustomLineItem();
    CustomLineItem(CustomPixmapItem *pStartItem, CustomPixmapItem *pEndItem);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
    int type() const Q_DECL_OVERRIDE { return Type;}

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *pEvent);

public:
    void setLineStartPixmapItem(CustomPixmapItem *pStartItem);
    void setLineEndPixmapItem(CustomPixmapItem *pEndItem);

    QPointF getStartPos();
    QPointF getEndPos();

    void updateLineItem();
    //画动画，从起点到终点
    void drawAnimation();

private:
    //设置线的风格
    void setLineStyle();
    //画线
    void drawLine();

private slots:
    void slotAnimationFinished();

private:
    CustomPixmapItem *m_pStartItem;
    CustomPixmapItem *m_pEndItem;

    QTimeLine *m_pTimer;
    QGraphicsItemAnimation *m_pAnimation;

    QGraphicsEllipseItem *m_pEllipseItem;//动画item
};

#endif // CUSTOMLINEITEM_H
