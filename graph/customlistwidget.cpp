﻿#include "customlistwidget.h"
#include "common.h"

#include <QDebug>
#include <QDrag>
#include <QMimeData>
#include <QMouseEvent>
#include <QDir>

CustomListWidget::CustomListWidget()
{
    init();
    setDragEnabled(true);

}

void CustomListWidget::init()
{
    QDir dir("./images");

    QStringList filters;
    filters << "*.png";
    dir.setNameFilters(filters);

    //遍历目录下png文件
    for (const auto &filename : dir.entryList()) {
        QString strFileNameRemoveSuffix = filename;
        strFileNameRemoveSuffix.remove(".png");
        QListWidgetItem *pListWidgetItem = new QListWidgetItem(QIcon(dir.absoluteFilePath(filename)), strFileNameRemoveSuffix);
        //存入图片的路径
        pListWidgetItem->setData(NODEPIXMAPPATH, dir.absoluteFilePath(filename));
        pListWidgetItem->setData(NODEPIXMAPNAME, strFileNameRemoveSuffix);
        addItem(pListWidgetItem);
    }
}

void CustomListWidget::mouseMoveEvent(QMouseEvent *event)
{
    QDrag *pDrag = new QDrag(this);
    QMimeData *pMimeData = new QMimeData;
    pMimeData->setText(itemAt(event->pos())->data(NODEPIXMAPPATH).toString());
    pMimeData->setData("NodeName", itemAt(event->pos())->data(NODEPIXMAPNAME).toByteArray());
    pDrag->setMimeData(pMimeData);
    pDrag->exec();

    QListWidget::mouseMoveEvent(event);
}

void CustomListWidget::mousePressEvent(QMouseEvent *event)
{
    QListWidget::mousePressEvent(event);
}
