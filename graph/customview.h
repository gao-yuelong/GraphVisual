﻿#ifndef CUSTOMVIEW_H
#define CUSTOMVIEW_H

#include <QGraphicsView>
#include <QObject>

#include "core/edge.h"
#include "customlineitem.h"
#include "custompixmapitem.h"

class CustomLineItem;

class CustomView : public QGraphicsView
{
    Q_OBJECT
public:
    CustomView();
    ~CustomView();

signals:
    void signalAddEdge(int, QPoint, QPoint);
    void signalAddLineItem(CustomLineItem *);
    void signalPixItemAddLine(CustomPixmapItem *, CustomPixmapItem *, CustomLineItem *);
    void signalUpdatePixItemLine(CustomPixmapItem *, QPoint, QPoint);

protected:
    void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;

    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

    void wheelEvent(QWheelEvent *) Q_DECL_OVERRIDE;

private:
    QPoint toScenePoint(const QPoint &);

private:
    QPoint m_pointSelectItem;   //选中的item
    QPointF m_pointStart;
    QPointF m_pointEnd;

    bool m_bPressItem;
    bool m_bNewEdge;
    qreal m_dScale;

    QLineF lineF;

    CustomLineItem *m_pLineItem;
    CustomPixmapItem *m_pPixItemStartItem;
    CustomPixmapItem *m_pPixItemEndItem;
};

#endif // CUSTOMVIEW_H
