﻿#ifndef CUSTOMITEM_H
#define CUSTOMITEM_H

#include "customlineitem.h"
#include <QGraphicsPixmapItem>
#include <QVector>
#include <QObject>

//结点
class CustomPixmapItem : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    CustomPixmapItem();

    enum { Type = UserType + 1 };

    QRectF boundingRect() const Q_DECL_OVERRIDE;
    QPainterPath shape() const Q_DECL_OVERRIDE;
    bool contains(const QPointF &point) const Q_DECL_OVERRIDE;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
    bool isObscuredBy(const QGraphicsItem *item) const Q_DECL_OVERRIDE;
    QPainterPath opaqueArea() const Q_DECL_OVERRIDE;
    int type() const Q_DECL_OVERRIDE {return Type;}

    void startRun();

signals:
    void signalSetStart();
    void signalSetEnd();

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *pEvent);
    QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;

public:
    void setPixItemId(int iId);
    int getPixItemId();

    void addLine(CustomLineItem *);

    void addStartLine(CustomLineItem *);
    void addEndLine(CustomLineItem *);

    QVector<CustomLineItem *> getLines();

    bool isStart() {
        return m_bIsStart;
    }
    bool isEnd() {
        return m_bIsEnd;
    }

    void setStart() {
        m_bIsStart = true;
        m_bIsEnd = false;
    }
    void setEnd() {
        m_bIsEnd = true;
        m_bIsStart =false;
    }

    void setNormal() {
        m_bIsEnd = false;
        m_bIsStart =false;
    }


private:
    QRectF getItemRect() const;

private:
    QVector<CustomLineItem *> m_vecStartLines;
    QVector<CustomLineItem *> m_vecEndLines;

    QVector<CustomLineItem *> m_vecLinesItem;//该结点的所有线

    bool m_bIsStart;
    bool m_bIsEnd;
};

#endif // CUSTOMITEM_H
