#ifndef LEFTSIDEBAR_H
#define LEFTSIDEBAR_H

#include <QWidget>

namespace Ui {
class LeftSideBar;
}

class LeftSideBar : public QWidget
{
    Q_OBJECT

public:
    explicit LeftSideBar(QWidget *parent = nullptr);
    ~LeftSideBar();

private:
    Ui::LeftSideBar *ui;
};

#endif // LEFTSIDEBAR_H
