﻿#include "customscene.h"

#include "src/log.h"
#include "core/node.h"

#include <QDebug>
#include <QLine>
#include <QGraphicsSceneDragDropEvent>
#include <QMimeData>
#include <QGraphicsPixmapItem>
#include <QKeyEvent>

CustomScene::CustomScene()
    : m_pGraphcsLineItem(nullptr)
    , m_pStartPixmapItem(nullptr)
    , m_pEndPixmapItem(nullptr)
    , m_iPixmapItemNumber(0)
{
    eMode = eMoveItem;
}

void CustomScene::setMode(CustomScene::E_MODE eMode)
{
    this->eMode = eMode;
}

void CustomScene::slotSetStartPixmapItem()
{
    auto item = dynamic_cast<CustomPixmapItem *>(sender());
    if (m_pStartPixmapItem == nullptr) m_pStartPixmapItem = item;
    if (item != m_pStartPixmapItem) {
        m_pStartPixmapItem->setNormal();
        m_pStartPixmapItem = item;
        m_pStartPixmapItem->setStart();
    } else {
        m_pStartPixmapItem->setStart();
    }
    emit signalStartItemChange(m_pStartPixmapItem->data(NODENAME).toString());
}

void CustomScene::slotSetEndPixmapItem()
{
    auto item = dynamic_cast<CustomPixmapItem *>(sender());
    if (m_pEndPixmapItem == nullptr) m_pEndPixmapItem = item;
    if (item != m_pEndPixmapItem) {
        m_pEndPixmapItem->setNormal();
        m_pEndPixmapItem = item;
        m_pEndPixmapItem->setEnd();
    } else {
        m_pEndPixmapItem->setEnd();
    }
    emit signalEndItemChange(m_pEndPixmapItem->data(NODENAME).toString());
}

void CustomScene::slotStartLoop()
{
    if (m_pStartPixmapItem) {
        m_pStartPixmapItem->startRun();
    }
}

void CustomScene::dragEnterEvent(QGraphicsSceneDragDropEvent *)
{
}

void CustomScene::dropEvent(QGraphicsSceneDragDropEvent *pEvent)
{
    if (pEvent->mimeData()->hasText()) {
        CustomPixmapItem *pPixmapItem = new CustomPixmapItem;
        pPixmapItem->setPixmap(QPixmap(pEvent->mimeData()->text()));
        pPixmapItem->setPos(pEvent->scenePos());

        pPixmapItem->setData(NODEPIXMAPPATH, pEvent->mimeData()->text());
        pPixmapItem->setData(NODEPIXMAPNAME, pEvent->mimeData()->data("NodeName"));

        addItem(pPixmapItem);

        pPixmapItem->setData(NODENAME, ++m_iPixmapItemNumber);

        connect(pPixmapItem, SIGNAL(signalSetStart()), this, SLOT(slotSetStartPixmapItem()));
        connect(pPixmapItem, SIGNAL(signalSetEnd()), this, SLOT(slotSetEndPixmapItem()));


        //添加node
        emit signalAddPixItem(pPixmapItem, pEvent->scenePos().toPoint());
    }
}

void CustomScene::dragMoveEvent(QGraphicsSceneDragDropEvent *)
{
}

void CustomScene::mouseMoveEvent(QGraphicsSceneMouseEvent *pEvent)
{
    if (eMode == eInsertLine && m_pGraphcsLineItem) {
        //疑问
        QLineF newLine(m_pGraphcsLineItem->line().p1(), pEvent->scenePos());
        m_pGraphcsLineItem->setLine(newLine);
    } else if (eMode == eMoveItem) {
        QGraphicsScene::mouseMoveEvent(pEvent);
    }
}

void CustomScene::mousePressEvent(QGraphicsSceneMouseEvent *pEvent)
{
    //如果当前没有点击item，则取消所有item的选中
//    QList<QGraphicsItem *> listItem = items(pEvent->scenePos());
//    if (listItem.isEmpty()) {
//        for (auto item : items(Qt::DescendingOrder)) {
//            item->setSelected(false);
//        }
//    }

    switch (eMode) {
    case eInsertLine: {
        m_pGraphcsLineItem = new QGraphicsLineItem(QLineF(pEvent->scenePos(), pEvent->scenePos()));
        addItem(m_pGraphcsLineItem);
    }break;
    case eMoveItem: {

    }break;
    }
    QGraphicsScene::mousePressEvent(pEvent);
}

void CustomScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *pEvent)
{
    if (m_pGraphcsLineItem && eMode == eInsertLine) {
        QList<QGraphicsItem *> startItems = items(m_pGraphcsLineItem->line().p1());
        if (startItems.count() && startItems.first() == m_pGraphcsLineItem)
            startItems.removeFirst();
        QList<QGraphicsItem *> endItems = items(m_pGraphcsLineItem->line().p2());
        if (endItems.count() && endItems.first() == m_pGraphcsLineItem)
            endItems.removeFirst();

        removeItem(m_pGraphcsLineItem);
        delete m_pGraphcsLineItem;

        if (startItems.count() > 0 && endItems.count() > 0 &&
            startItems.first()->type() == CustomPixmapItem::Type &&
            endItems.first()->type() == CustomPixmapItem::Type &&
            startItems.first() != endItems.first()) {
            CustomPixmapItem *pStartItem = qgraphicsitem_cast<CustomPixmapItem *>(startItems.first());
            CustomPixmapItem *pEndItem = qgraphicsitem_cast<CustomPixmapItem *>(endItems.first());
            CustomLineItem *pLineItem = new CustomLineItem(pStartItem, pEndItem);
            //pLineItem->setColor(myLineColor);
            pStartItem->addStartLine(pLineItem);
            pEndItem->addEndLine(pLineItem);
            //arrow->setZValue(-1000.0);
            addItem(pLineItem);
            pLineItem->updateLineItem();
        }
    }
    m_pGraphcsLineItem = 0;
    QGraphicsScene::mouseReleaseEvent(pEvent);
}


