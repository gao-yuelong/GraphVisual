﻿#include "graphvisual.h"
#include "ui_graphvisual.h"

#include <QDebug>
#include <QPixmap>
#include <QGraphicsView>
#include <QMenuBar>
#include <QVector>
#include <QMessageBox>

#include "customlistwidget.h"
#include "customscene.h"
#include "customview.h"
#include "src/log.h"
#include "core/node.h"
#include "garden.h"

GraphVisual::GraphVisual(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GraphVisual),
    m_pSplitter(nullptr),
    m_pListWidget(nullptr),
    m_pScene(nullptr),
    m_pView(nullptr),
    m_pGarden(new Garden()),
    m_pWidget(nullptr)
{
    ui->setupUi(this);

    init();
    bindSignalSlot();
}

GraphVisual::~GraphVisual()
{
    delete m_pSplitter;
    delete m_pListWidget;
    delete m_pScene;
    delete m_pHboxLayout;
    delete ui;
}


void GraphVisual::closeEvent(QCloseEvent *)
{
    LOG_INFO("bye!")
}

void GraphVisual::keyPressEvent(QKeyEvent *pEvent)
{
    if (pEvent->key() == Qt::Key_I) {
        setCrossCursor();
    } else if (pEvent->key() == Qt::Key_Space) {
        setArrowCursor();
    }
}

void GraphVisual::slotSceneScaleChanged(const QString &strScale)
{
    double newScale = strScale.left(strScale.indexOf(tr("%"))).toDouble() / 100.0;
    QMatrix oldMatrix = m_pView->matrix();
    m_pView->resetMatrix();
    m_pView->translate(oldMatrix.dx(), oldMatrix.dy());
    m_pView->scale(newScale, newScale);
}

void GraphVisual::init()
{
    m_pWidget = new QWidget();
    QHBoxLayout *pHboxLayout = new QHBoxLayout();
    m_pWidget->setLayout(pHboxLayout);

    m_pView = new CustomView();
    pHboxLayout->addWidget(m_pView);
    pHboxLayout->setMargin(0);

    m_pSplitter = new QSplitter(this);
    m_pListWidget = new CustomListWidget();
    m_pSplitter->addWidget(m_pListWidget);
    m_pSplitter->addWidget(m_pWidget);
    m_pSplitter->addWidget(ui->stackedWidget);

    m_pSplitter->setStretchFactor(0, 1);
    m_pSplitter->setStretchFactor(1, 6);
    m_pSplitter->setStretchFactor(2, 2);

    m_pHboxLayout = new QHBoxLayout();
    m_pHboxLayout->addWidget(m_pSplitter);
    m_pHboxLayout->setMargin(1);
    ui->wgtMain->setLayout(m_pHboxLayout);

    //scene
    m_pScene = new CustomScene();
    m_pScene->setSceneRect(QRectF(0, 0, 5000, 5000));
    m_pView->setScene(m_pScene);
    //qDebug() << "m_pView->rect()" << m_pView->rect();
    //m_pView->setSceneRect(m_pView->rect());

    createMenuBar();

    QStringList strListScale;
    strListScale << tr("50%") << tr("75%") << tr("100%") << tr("125%") << tr("150%");
    ui->comBoxSceneScale->addItems(strListScale);
    ui->comBoxSceneScale->setCurrentIndex(2);

    setFocusPolicy(Qt::StrongFocus);
}

void GraphVisual::bindSignalSlot()
{
    connect(m_pScene, SIGNAL(signalAddPixItem(CustomPixmapItem *, QPoint)), m_pGarden, SLOT(slotAddPixItem(CustomPixmapItem *, QPoint)));
    connect(m_pScene, SIGNAL(signalStartItemChange(QString)), this, SLOT(slotStartItemChange(QString)));
    connect(m_pScene, SIGNAL(signalEndItemChange(QString)), this, SLOT(slotEndItemChange(QString)));

    connect(this, SIGNAL(signalStartLoop()), m_pScene, SLOT(slotStartLoop()));

    connect(m_pView, SIGNAL(signalAddLineItem(CustomLineItem*)), m_pGarden, SLOT(slotAddLineItem(CustomLineItem*)));

    connect(m_pView, SIGNAL(signalAddEdge(int,QPoint,QPoint)), this, SLOT(slotAddEdge(int,QPoint,QPoint)));

    connect(m_pView, SIGNAL(signalPixItemAddLine(CustomPixmapItem *, CustomPixmapItem *, CustomLineItem *)),
            this, SLOT(slotPixItemAddLine(CustomPixmapItem *, CustomPixmapItem *, CustomLineItem *)));

    connect(m_pView, SIGNAL(signalUpdatePixItemLine(CustomPixmapItem *, QPoint, QPoint)),
            this, SLOT(slotUpdatePixItemLine(CustomPixmapItem *, QPoint, QPoint)));

    connect(ui->comBoxSceneScale, SIGNAL(currentIndexChanged(QString)),
            this, SLOT(slotSceneScaleChanged(QString)));
}

void GraphVisual::outputAllNodeId()
{
    //遍历set
//    qDebug().noquote() << "=====outputAllNodeId begin======";
//    for (auto pixItem : m_setPixItemSet)
//    {
//        qDebug() << pixItem->getPixItemId();
//    }
//    qDebug() << "=====outputAllNodeId end======";
}

void GraphVisual::outputAllEdgeId()
{
    //遍历set
//    qDebug().noquote() << "=====outputAllEdgeId begin======";
////    for (auto edge : m_setEdgeSet)
////    {
////        qDebug() << edge->getEdgeId();
////    }
//    qDebug() << "=====outputAllEdgeId end======";
}

void GraphVisual::outputPixItemLines()
{
    //输出所有结点的边
//    for (auto pixitem : m_setPixItemSet) {
//        qDebug() << "pixitem begin===" << endl;
//        //qDebug() << pixitem->getLines();
//        //        for (auto l : pixitem->getLines()) {
////            qDebug
////        }
//        qDebug() << "pixitem end=====" << endl;

    //    }
}

void GraphVisual::setCrossCursor()
{
    setCursor(Qt::CrossCursor);
    m_pScene->setMode(CustomScene::eInsertLine);
}

void GraphVisual::setArrowCursor()
{
    setCursor(Qt::ArrowCursor);
    m_pScene->setMode(CustomScene::eMoveItem);
}

//void GraphVisual::slotAddPixItem(CustomPixmapItem *pPixItem, QPoint pointPos)
//{
//    LOG_INFO("add node")
//    static int iNodeNumber = 1;
//    pPixItem->setPixItemId(iNodeNumber);
//    iNodeNumber++;

//    //Node *pNode = new Node(iNodeId);
//    //pNode->setPos(pointPos);

//    m_setPixItemSet.insert(pPixItem);

//    outputAllNodeId();
//}

void GraphVisual::slotPixItemAddLine(CustomPixmapItem *pPixItemStart, CustomPixmapItem *pPixItemEnd, CustomLineItem *pLineItem)
{
    //开始结点和结束结点添加线
//    qDebug() << __FUNCTION__;
//    qDebug() << pPixItemStart;
//    pPixItemStart->addLine(pLineItem);
//    qDebug() << __FUNCTION__;
//    qDebug() << pPixItemEnd;
//    pPixItemEnd->addLine(pLineItem);

}

void GraphVisual::slotUpdatePixItemLine(CustomPixmapItem *pPixItem, QPoint pointStart, QPoint pointEnd)
{
    //更新pPixItem所有线的位置
    qDebug() << "slotUpdatePixItemLine";
    outputPixItemLines();
}

void GraphVisual::slotAddEdge(int iEdgeId, QPoint pointStart, QPoint pointEnd)
{
    LOG_INFO("add edge")
//    Edge *pEdge = new Edge(iEdgeId);
//    pEdge->setStartPos(pointStart);
//    pEdge->setEndPos(pointEnd);

    //m_setEdgeSet.insert(pEdge);
    outputAllEdgeId();
}

void GraphVisual::slotStartItemChange(QString strStartItemText)
{
    ui->lbStartItemText->setText(strStartItemText);
}

void GraphVisual::slotEndItemChange(QString strEndItemText)
{
    ui->lbEndItemText->setText(strEndItemText);
}

void GraphVisual::createMenuBar()
{
    QMenuBar *pMenuBar = new QMenuBar(this);

    QMenu *pFileMenu = new QMenu("File");
    QMenu *pHelpMenu = new QMenu("Help");
    pHelpMenu->addAction(
                QObject::tr("About"), [this](){
        QMessageBox::about(this, "About", QStringLiteral("一个图可视化展示软件\n作者：高二\n微信公众号：高二的笔记"));
    });

    pMenuBar->addMenu(pFileMenu);
    pMenuBar->addMenu(pHelpMenu);

    pMenuBar->setFixedHeight(23);
    ui->horizontalLayout_3->addWidget(pMenuBar);
}

void GraphVisual::on_btnLoop_clicked()
{
    //点击循环
    emit signalStartLoop();
}

void GraphVisual::on_tBtnSelect_clicked()
{
    setArrowCursor();
}

void GraphVisual::on_tBtnInsertLine_clicked()
{
    setCrossCursor();
}

void GraphVisual::on_tBtnClear_clicked()
{
    m_pScene->clear();
}

void GraphVisual::on_treeWidget_itemDoubleClicked(QTreeWidgetItem *pItem, int iColumn)
{
    if (iColumn == 0) {
        ui->stackedWidget->setCurrentIndex(1);
    }
}
