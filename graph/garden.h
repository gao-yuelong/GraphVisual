﻿#ifndef GARDEN_H
#define GARDEN_H

#include <QPoint>
#include <QObject>
#include <QSet>

class CustomPixmapItem;
class CustomLineItem;
//园地（保存所有的pixmapitem和lineitem）
class Garden : public QObject
{
    Q_OBJECT
public:
    Garden();

private slots:
    void slotAddPixItem(CustomPixmapItem *, QPoint);
    void slotAddLineItem(CustomLineItem *);

private:
    QSet<CustomPixmapItem *> m_setPixItemSet;
    QSet<CustomLineItem *> m_setLineItemSet;
};

#endif // GARDEN_H
