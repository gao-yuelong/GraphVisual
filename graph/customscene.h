﻿#ifndef CUSTOMSCENE_H
#define CUSTOMSCENE_H

#include "common.h"
#include "custompixmapitem.h"
#include "customlineitem.h"

#include <QGraphicsScene>
#include <QGraphicsLineItem>
#include <QObject>

class CustomScene : public QGraphicsScene
{
    Q_OBJECT
public:
    enum E_MODE {
        eInsertLine,
        eMoveItem
    };

    CustomScene();

    void setMode(E_MODE);

signals:
    void signalStartItemChange(QString);
    void signalEndItemChange(QString);

    void signalAddPixItem(CustomPixmapItem *, QPoint);

private slots:
    void slotSetStartPixmapItem();
    void slotSetEndPixmapItem();

    void slotStartLoop();

protected:
    void dragEnterEvent(QGraphicsSceneDragDropEvent *) Q_DECL_OVERRIDE;
    void dropEvent(QGraphicsSceneDragDropEvent *) Q_DECL_OVERRIDE;
    void dragMoveEvent(QGraphicsSceneDragDropEvent *) Q_DECL_OVERRIDE;

    void mouseMoveEvent(QGraphicsSceneMouseEvent *) Q_DECL_OVERRIDE;
    void mousePressEvent(QGraphicsSceneMouseEvent *) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *) Q_DECL_OVERRIDE;

private:

    E_MODE eMode;
    QGraphicsLineItem *m_pGraphcsLineItem;//临时lineItem

    CustomPixmapItem *m_pStartPixmapItem;
    CustomPixmapItem *m_pEndPixmapItem;

    int m_iPixmapItemNumber; //当前所有pixmapitem的数量

};

#endif // CUSTOMSCENE_H
