﻿#ifndef GRAPHVISUAL_H
#define GRAPHVISUAL_H

#include <QWidget>
#include <QSplitter>
#include <QListWidget>
#include <QHBoxLayout>
#include <QDragMoveEvent>
#include <QTreeWidgetItem>
#include <QSet>

#include "customlineitem.h"
#include "custompixmapitem.h"

namespace Ui {
class GraphVisual;
}

class CustomListWidget;
class CustomScene;
class CustomView;
class QMenuBar;
class Garden;

class GraphVisual : public QWidget
{
    Q_OBJECT

public:
    explicit GraphVisual(QWidget *parent = nullptr);
    ~GraphVisual();

    void init();

    void bindSignalSlot();

signals:
    void signalStartLoop();

protected:
    void closeEvent(QCloseEvent *) Q_DECL_OVERRIDE;
    void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;

private slots:
    void slotSceneScaleChanged(const QString &strScale);

    void slotPixItemAddLine(CustomPixmapItem *, CustomPixmapItem *, CustomLineItem *);
    void slotUpdatePixItemLine(CustomPixmapItem *, QPoint, QPoint);


    void slotAddEdge(int, QPoint, QPoint);

    void slotStartItemChange(QString);
    void slotEndItemChange(QString);

    void on_btnLoop_clicked();

    void on_tBtnSelect_clicked();

    void on_tBtnInsertLine_clicked();

    void on_tBtnClear_clicked();

    void on_treeWidget_itemDoubleClicked(QTreeWidgetItem *, int);

private:

    void createMenuBar();

    void outputAllNodeId();
    void outputAllEdgeId();
    void outputPixItemLines();

    void setCrossCursor();
    void setArrowCursor();

private:
    Ui::GraphVisual *ui;

    QSplitter *m_pSplitter;         //分割窗口
    CustomListWidget *m_pListWidget;     //listwidget
    CustomScene *m_pScene;             //显示scene
    CustomView *m_pView;
    QWidget *m_pWidget;

    QHBoxLayout *m_pHboxLayout;

    Garden *m_pGarden; //园地
};

#endif // GRAPHVISUAL_H
